import torch
import torch.nn as nn
import torch.optim as optim
from sklearn.metrics import accuracy_score
from sklearn.model_selection import KFold


class FFN(nn.Module):
    def __init__(self, input_dim):
        super(FFN, self).__init__()
        self.fc1 = nn.Linear(input_dim, 128)
        self.fc2 = nn.Linear(128, 64)
        self.fc3 = nn.Linear(64, 1)

    def forward(self, x):
        x = torch.relu(self.fc1(x))
        x = torch.relu(self.fc2(x))
        x = torch.sigmoid(self.fc3(x))
        return x


class FeedForwardNNModel:
    def __init__(self, input_dim, lr=0.001):
        self.model = FFN(input_dim)
        self.criterion = nn.BCELoss()
        self.optimizer = optim.Adam(self.model.parameters(), lr=lr)

    def train(self, X_train, y_train, epochs=10):
        X_train = torch.tensor(X_train, dtype=torch.float32)
        y_train = torch.tensor(y_train, dtype=torch.float32).view(-1, 1)

        for _ in range(epochs):
            self.model.train()
            self.optimizer.zero_grad()
            outputs = self.model(X_train)
            loss = self.criterion(outputs, y_train)
            loss.backward()
            self.optimizer.step()

    def cross_validate(self, X, y, cv=5, epochs=10):
        kf = KFold(n_splits=cv)
        scores = []

        for train_index, val_index in kf.split(X):
            X_train, X_val = X[train_index], X[val_index]
            y_train, y_val = y[train_index], y[val_index]

            self.train(X_train, y_train, epochs)
            y_pred = self.predict(X_val)
            scores.append(accuracy_score(y_val, y_pred))

        return sum(scores) / len(scores)

    def predict(self, X):
        X = torch.tensor(X, dtype=torch.float32)
        self.model.eval()
        with torch.no_grad():
            outputs = self.model(X)
        return outputs.numpy().round().flatten()

    def save(self, path):
        torch.save(self.model.state_dict(), path)

    def load(self, path, input_dim):
        self.model = FFN(input_dim)
        self.model.load_state_dict(torch.load(path))
