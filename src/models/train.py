import argparse
import pandas as pd
from sklearn.metrics import accuracy_score
import mlflow
import mlflow.sklearn

from src.models.ffn_model import FeedForwardNNModel
from src.models.log_reg_model import LogisticRegressionModel
from src.models.decision_tree_model import DecisionTreeModel


def load_data(features_type):
    if features_type == "tfidf":
        tfidf_features_a = pd.read_csv("data/processed/tfidf_features_a.csv")
        tfidf_features_b = pd.read_csv("data/processed/tfidf_features_b.csv")
        return (
            pd.concat([tfidf_features_a, tfidf_features_b], axis=1),
            "data/raw/train.csv",
        )
    elif features_type == "bert":
        bert_features_a = pd.read_csv("data/processed/bert_features_a.csv")
        bert_features_b = pd.read_csv("data/processed/bert_features_b.csv")
        return (
            pd.concat([bert_features_a, bert_features_b], axis=1),
            "data/raw/train.csv",
        )
    elif features_type == "both":
        tfidf_features_a = pd.read_csv("data/processed/tfidf_features_a.csv")
        tfidf_features_b = pd.read_csv("data/processed/tfidf_features_b.csv")
        bert_features_a = pd.read_csv("data/processed/bert_features_a.csv")
        bert_features_b = pd.read_csv("data/processed/bert_features_b.csv")
        return (
            pd.concat(
                [tfidf_features_a, tfidf_features_b, bert_features_a, bert_features_b],
                axis=1,
            ),
            "data/raw/train.csv",
        )
    else:
        raise ValueError("Invalid features type provided.")


def main(model_type, features_type, epochs):
    with mlflow.start_run():
        mlflow.log_param("model_type", model_type)
        mlflow.log_param("features_type", features_type)
        mlflow.log_param("epochs", epochs if model_type == "ffn" else 1)

        X, data_path = load_data(features_type)
        data = pd.read_csv(data_path)[:100]  # Ограничение данных для тестирования
        y = data["winner_model_a"]

        if model_type == "log_reg":
            model = LogisticRegressionModel()
        elif model_type == "decision_tree":
            model = DecisionTreeModel()
        elif model_type == "ffn":
            input_dim = X.shape[1]
            model = FeedForwardNNModel(input_dim)
        else:
            raise ValueError("Invalid model type provided.")

        print("Cross-Validating...")
        cv_score = model.cross_validate(
            X.values, y.values, epochs=epochs if model_type == "ffn" else 1
        )
        print(f"Cross-Validation Accuracy: {cv_score}")
        mlflow.log_metric("cv_accuracy", cv_score)

        print("Training...")
        model.train(X.values, y.values, epochs=epochs if model_type == "ffn" else 1)
        y_pred = model.predict(X.values)
        train_accuracy = accuracy_score(y, y_pred)
        print(f"Training Accuracy: {train_accuracy}")
        mlflow.log_metric("training_accuracy", train_accuracy)

        model_path = f"models/{model_type}_model.pkl"
        print("Saving model...")
        model.save(model_path)
        mlflow.sklearn.log_model(model, "model", registered_model_name=model_type)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--model",
        type=str,
        choices=["log_reg", "decision_tree", "ffn"],
        required=True,
        help="Choose the model to train: 'log_reg', 'decision_tree', 'ffn'",
    )
    parser.add_argument(
        "--features",
        type=str,
        choices=["tfidf", "bert", "both"],
        required=True,
        help="Choose the type of features to use: 'tfidf', 'bert', 'both'",
    )
    parser.add_argument(
        "--epochs",
        type=int,
        default=10,
        help="Number of epochs for training (only relevant for FFN)",
    )

    args = parser.parse_args()

    mlflow.create_experiment('dad', artifact_location="s3://mlflow")
    # mlflow.set_tracking_uri("http://147.45.164.31:5000")
    mlflow.set_tracking_uri("http://localhost:5000")
    main(args.model, args.features, args.epochs)
