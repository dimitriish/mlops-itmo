from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import cross_val_score
import joblib


class LogisticRegressionModel:
    def __init__(self):
        self.model = LogisticRegression()

    def train(self, X_train, y_train):
        self.model.fit(X_train, y_train)

    def cross_validate(self, X_train, y_train, cv=5):
        scores = cross_val_score(self.model, X_train, y_train, cv=cv)
        return scores.mean()

    def predict(self, X):
        return self.model.predict(X)

    def save(self, path):
        joblib.dump(self.model, path)

    def load(self, path):
        self.model = joblib.load(path)
