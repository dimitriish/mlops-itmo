import pandas as pd


def main():
    raw_data_path = "data/raw/train.csv"
    prepared_data_path = "data/raw/train_100.csv"

    df = pd.read_csv(raw_data_path)
    df[:100].to_csv(prepared_data_path, index=False)


if __name__ == "__main__":
    main()
