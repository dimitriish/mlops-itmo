import pandas as pd
from transformers import BertTokenizer, BertModel
import torch


class BertFeatureGenerator:
    def __init__(self, model_name="bert-base-uncased"):
        self.tokenizer = BertTokenizer.from_pretrained(model_name)
        self.model = BertModel.from_pretrained(model_name)

    def encode_text(self, text):
        inputs = self.tokenizer(
            text,
            return_tensors="pt",
            max_length=512,
            truncation=True,
            padding=True
        )
        with torch.no_grad():
            outputs = self.model(**inputs)
        return outputs.last_hidden_state.mean(dim=1).squeeze().numpy()

    def transform(self, data, column):
        features = data[column].apply(self.encode_text)
        return features.tolist()


def main():
    data = pd.read_csv("data/raw/train_100.csv")

    bert_generator = BertFeatureGenerator()
    bert_features_a = bert_generator.transform(data, "response_a")
    bert_features_b = bert_generator.transform(data, "response_b")

    pd.DataFrame(bert_features_a).to_csv(
        "data/processed/bert_features_a.csv", index=False
    )
    pd.DataFrame(bert_features_b).to_csv(
        "data/processed/bert_features_b.csv", index=False
    )


if __name__ == "__main__":
    main()
