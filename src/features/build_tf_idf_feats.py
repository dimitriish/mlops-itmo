import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer


class TfidfFeatureGenerator:
    def __init__(self, max_features=1000):
        self.vectorizer = TfidfVectorizer(max_features=max_features)

    def fit_transform(self, data, column):
        tfidf_matrix = self.vectorizer.fit_transform(data[column])
        return tfidf_matrix

    def transform(self, data, column):
        tfidf_matrix = self.vectorizer.transform(data[column])
        return tfidf_matrix


def main():
    data = pd.read_csv("data/raw/train_100.csv")

    tfidf_generator = TfidfFeatureGenerator()
    tfidf_matrix_a = tfidf_generator.fit_transform(data, "response_a")
    tfidf_matrix_b = tfidf_generator.fit_transform(data, "response_b")

    pd.DataFrame(tfidf_matrix_a.toarray()).to_csv(
        "data/processed/tfidf_features_a.csv", index=False
    )
    pd.DataFrame(tfidf_matrix_b.toarray()).to_csv(
        "data/processed/tfidf_features_b.csv", index=False
    )


if __name__ == "__main__":
    main()
