import pytest
import numpy as np
from src.models.ffn_model import FeedForwardNNModel


@pytest.fixture
def data():
    X = np.random.rand(100, 20)
    y = np.random.randint(0, 2, 100)
    return X, y


def test_ffn_train(data):
    X, y = data
    model = FeedForwardNNModel(input_dim=20)
    model.train(X, y, epochs=1)
    assert model.model is not None


def test_ffn_predict(data):
    X, y = data
    model = FeedForwardNNModel(input_dim=20)
    model.train(X, y, epochs=1)
    y_pred = model.predict(X)
    assert y_pred.shape == y.shape


def test_ffn_save_load(data, tmp_path):
    X, y = data
    model = FeedForwardNNModel(input_dim=20)
    model.train(X, y, epochs=1)
    model.save(tmp_path / "ffn_model.pth")

    loaded_model = FeedForwardNNModel(input_dim=20)
    loaded_model.load(tmp_path / "ffn_model.pth", input_dim=20)

    y_pred = loaded_model.predict(X)
    assert y_pred.shape == y.shape
