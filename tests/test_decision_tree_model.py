import pytest
import numpy as np
from src.models.decision_tree_model import DecisionTreeModel


@pytest.fixture
def data():
    X = np.random.rand(100, 20)
    y = np.random.randint(0, 2, 100)
    return X, y


def test_decision_tree_train(data):
    X, y = data
    model = DecisionTreeModel()
    model.train(X, y)
    assert model.model is not None


def test_decision_tree_predict(data):
    X, y = data
    model = DecisionTreeModel()
    model.train(X, y)
    y_pred = model.predict(X)
    assert y_pred.shape == y.shape


def test_decision_tree_save_load(data, tmp_path):
    X, y = data
    model = DecisionTreeModel()
    model.train(X, y)
    model.save(tmp_path / "dec_tree_model.pkl")

    loaded_model = DecisionTreeModel()
    loaded_model.load(tmp_path / "dec_tree_model.pkl")

    y_pred = loaded_model.predict(X)
    assert y_pred.shape == y.shape
