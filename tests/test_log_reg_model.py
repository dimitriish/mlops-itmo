import pytest
import numpy as np
from src.models.log_reg_model import LogisticRegressionModel


@pytest.fixture
def data():
    X = np.random.rand(100, 20)
    y = np.random.randint(0, 2, 100)
    return X, y


def test_log_reg_train(data):
    X, y = data
    model = LogisticRegressionModel()
    model.train(X, y)
    assert model.model is not None


def test_log_reg_predict(data):
    X, y = data
    model = LogisticRegressionModel()
    model.train(X, y)
    y_pred = model.predict(X)
    assert y_pred.shape == y.shape


def test_log_reg_save_load(data, tmp_path):
    X, y = data
    model = LogisticRegressionModel()
    model.train(X, y)
    model.save(tmp_path / "log_reg_model.pkl")

    loaded_model = LogisticRegressionModel()
    loaded_model.load(tmp_path / "log_reg_model.pkl")

    y_pred = loaded_model.predict(X)
    assert y_pred.shape == y.shape
